package tw.cookie.orm.query;

import tw.cookie.orm.query.criterion.Criterion;
import tw.cookie.orm.query.criterion.Operator;
import tw.cookie.orm.query.criterion.expression.*;
import tw.cookie.orm.query.criterion.junction.Conjunction;
import tw.cookie.orm.query.criterion.junction.Disjunction;
import tw.cookie.orm.query.criterion.junction.Junction;
import tw.cookie.orm.translator.Translator;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract Query Translation. Convert a generic Query with nested criteria to a data store specific query. Extend to
 * provide data store specific query component implementations
 *
 * @param <IN> the INPUT type for example Scan
 * @param <OUT> the OUTPUT type for example Scan
 * @param <TRANS> the Criterion will be translate type for example Filter
 */
public abstract class AbstractQueryTranslator<IN extends Query, OUT, TRANS> implements Translator<IN, OUT> {

    private final Class<IN> inClass;
    private final Class<OUT> outClass;
    private final Class<TRANS> transClass;

    public AbstractQueryTranslator(Class<IN> inClass, Class<OUT> outClass, Class<TRANS> transClass) {
        this.inClass = inClass;
        this.outClass = outClass;
        this.transClass = transClass;
    }

    protected TRANS translate(Criterion c) {
        // a Criterion can be an Expression or a Junction
        if (c instanceof Expression) {
            return translate((Expression) c);
        } else if (c instanceof Junction) {
            return translate((Junction) c);
        } else {
            throw unsupported(c.getClass());
        }
    }

    protected TRANS translate(Expression e) {
        if (e instanceof EqualityExpression) {
            return translate((EqualityExpression) e, e.getPropertyName());
        } else if (e instanceof EqualityIdExpression) {
            return translate((EqualityIdExpression) e, e.getPropertyName());
        } else if (e instanceof RangeExpression) {
            return translate((RangeExpression) e, e.getPropertyName());
        } else if (e instanceof SetExpression) {
            return translate((SetExpression) e, e.getPropertyName());
        } else if (e instanceof UnaryExpression) {
            return translate((UnaryExpression) e, e.getPropertyName());
        } else {
            throw unsupported(e.getClass());
        }
    }

    protected TRANS translate(Junction j) {
        // a Junction can be a Conjunction (and) or a Disjunction (or)
        if (j instanceof Conjunction) {
            return translate((Conjunction) j);
        } else if (j instanceof Disjunction) {
            return translate((Disjunction) j);
        } else {
            throw unsupported(j.getClass());
        }
    }

    protected TRANS translate(Conjunction j) {
        return and(subQueries(j));
    }

    protected TRANS translate(Disjunction j) {
        return or(subQueries(j));
    }

    @SuppressWarnings("unchecked")
    private TRANS[] subQueries(Junction j) {
        List<Criterion> criteria = j.getCriteria();
        List<TRANS> translated = new ArrayList<>(criteria.size());
        for (Criterion c : criteria) {
            TRANS q = translate(c);
            if (q != null) {
                translated.add(q);
            }
        }
        return translated.toArray((TRANS[]) Array.newInstance(transClass, translated.size()));
    }

    protected TRANS translate(EqualityExpression e, String fieldName) {
        Operator operator = e.getOperator();
        Object value = e.getValue();
        switch (operator) {
            case EQUAL:
                return eq(fieldName, value);
            case NOT_EQUAL:
                return ne(fieldName, value);
            case GREATER_THAN:
                return gt(fieldName, value);
            case GREATER_THAN_OR_EQUAL:
                return gte(fieldName, value);
            case LESS_THAN:
                return lt(fieldName, value);
            case LESS_THAN_OR_EQUAL:
                return lte(fieldName, value);
            case SUB:
                return substring(fieldName, value);
            case REGEX:
                return regexString(fieldName, value);
            default:
                throw unsupported(operator, EqualityExpression.class);
        }
    }

    private TRANS translate(EqualityIdExpression e, String fieldName) {
        Operator operator = e.getOperator();
        Object value = e.getValue();
        switch (operator) {
            case SUB:
                return substringId(fieldName, value);
            case REGEX:
                return regexStringId(fieldName, value);
            default:
                throw unsupported(operator, EqualityExpression.class);
        }
    }

    protected TRANS translate(RangeExpression e, String fieldName) {
        Operator operator = e.getOperator();
        Object from = e.getFrom();
        Object to = e.getTo();
        switch (operator) {
            case BETWEEN:
                return between(fieldName, from, to);
            default:
                throw unsupported(operator, RangeExpression.class);
        }
    }

    protected TRANS translate(SetExpression e, String fieldName) {
        Operator operator = e.getOperator();
        Object[] values = e.getValues();
        switch (operator) {
            case IN:
                return in(fieldName, values);
            case NOT_IN:
                return notIn(fieldName, values);
            default:
                throw unsupported(operator, SetExpression.class);
        }
    }

    protected TRANS translate(UnaryExpression e, String fieldName) {
        Operator operator = e.getOperator();
        switch (operator) {
            case NULL:
                return isNull(fieldName);
            case NOT_NULL:
                return notNull(fieldName);
            case EMPTY:
                return isEmpty(fieldName);
            case NOT_EMPTY:
                return notEmpty(fieldName);
            default:
                throw unsupported(operator, UnaryExpression.class);
        }
    }

    private UnsupportedOperationException unsupported(Class<? extends Criterion> type) {
        throw new UnsupportedOperationException(type.getSimpleName() + " type not supported.");
    }

    private UnsupportedOperationException unsupported(Operator operator, Class<? extends Expression> expressionType) {
        throw new UnsupportedOperationException(operator + " not supported for " + expressionType.getSimpleName());
    }

    /**
     * Translate an "equal" expression
     *
     * @param fieldName the resolved field name
     * @param value     the reference value
     * @return TRANS
     */
    protected abstract TRANS eq(String fieldName, Object value);

    /**
     * Translate a "not equal" expression
     *
     * @param fieldName the resolved field name
     * @param value     the reference value
     * @return TRANS
     */
    protected abstract TRANS ne(String fieldName, Object value);

    /**
     * Translate a "less than" expression
     *
     * @param fieldName the resolved field name
     * @param value     the reference value
     * @return TRANS
     */
    protected abstract TRANS lt(String fieldName, Object value);

    /**
     * Translate a "less than or equal" expression
     *
     * @param fieldName the resolved field name
     * @param value     the reference value
     * @return TRANS
     */
    protected abstract TRANS lte(String fieldName, Object value);

    /**
     * Translate a "greater than" expression
     *
     * @param fieldName the resolved field name
     * @param value     the reference value
     * @return TRANS
     */
    protected abstract TRANS gt(String fieldName, Object value);

    /**
     * Translate a "greater than or equal" expression
     *
     * @param fieldName the resolved field name
     * @param value     the reference value
     * @return TRANS
     */
    protected abstract TRANS gte(String fieldName, Object value);

    /**
     * Translate a "between" expression
     *
     * @param fieldName the resolved field name
     * @param from      the lower bound value
     * @param to        the upper bound value
     * @return TRANS
     */
    protected abstract TRANS between(String fieldName, Object from, Object to);

    /**
     * Translate an "in" expression
     *
     * @param fieldName the resolved field name
     * @param values    the reference values
     * @return TRANS
     */
    protected abstract TRANS in(String fieldName, Object[] values);

    /**
     * Translate a "not in" expression
     *
     * @param fieldName the resolved field name
     * @param values    the reference values
     * @return TRANS
     */
    protected abstract TRANS notIn(String fieldName, Object[] values);

    /**
     * Translate a "is null" expression
     *
     * @param fieldName the resolved field name
     * @return TRANS
     */
    protected abstract TRANS isNull(String fieldName);

    /**
     * Translate a "not null" expression
     *
     * @param fieldName the resolved field name
     * @return TRANS
     */
    protected abstract TRANS notNull(String fieldName);

    /**
     * Translate a "is empty" expression
     *
     * @param fieldName the resolved field name
     * @return TRANS
     */
    protected abstract TRANS isEmpty(String fieldName);

    /**
     * Translate a "is empty" expression
     *
     * @param fieldName the resolved field name
     * @return TRANS
     */
    protected abstract TRANS notEmpty(String fieldName);

    protected abstract TRANS regexString(String fieldName, Object value);

    protected abstract TRANS substring(String fieldName, Object value);

    protected abstract TRANS regexStringId(String fieldName, Object value);

    protected abstract TRANS substringId(String fieldName, Object value);

    /**
     * Translate an "and" expression
     *
     * @param trans TRANS
     * @return TRANS
     */
    @SuppressWarnings("unchecked")
    protected abstract TRANS and(TRANS... trans);

    /**
     * Translate an "or" expression
     *
     * @param trans TRANS
     * @return TRANS
     */
    @SuppressWarnings("unchecked")
    protected abstract TRANS or(TRANS... trans);
}
