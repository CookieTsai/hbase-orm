package tw.cookie.orm.query;


import tw.cookie.orm.query.criterion.Criterion;

/**
 * A generic, object oriented representation of a query
 */
public interface Query {

    /**
     * Get the query criteria. The top level object can either be a single Criterion or a Junction of multiple nested
     * Criterion objects.
     *
     * @return the root criterion node
     */
    Criterion getCriteria();
}
