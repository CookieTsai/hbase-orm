package tw.cookie.orm.query;

public interface QueryBuilder<OUT> {
    OUT build();
}
