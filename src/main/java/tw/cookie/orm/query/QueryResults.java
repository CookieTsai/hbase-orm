package tw.cookie.orm.query;

public interface QueryResults {
    <R> QueryResult<R> getEntity(Class<R> type);
    <R> int getEntitySize(Class<R> type);
}
