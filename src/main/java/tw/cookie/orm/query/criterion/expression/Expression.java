package tw.cookie.orm.query.criterion.expression;

import tw.cookie.orm.query.criterion.Criterion;
import tw.cookie.orm.query.criterion.Operator;
import tw.cookie.orm.query.criterion.WithOperator;
import tw.cookie.orm.query.criterion.WithProperty;

public abstract class Expression implements Criterion, WithOperator, WithProperty {
    private final Operator operator;
    private final String propertyName;

    protected Expression(Operator operator, String propertyName) {
        this.operator = operator;
        this.propertyName = propertyName;
    }

    @Override
    public Operator getOperator() {
        return operator;
    }

    @Override
    public String getPropertyName() {
        return propertyName;
    }
}
