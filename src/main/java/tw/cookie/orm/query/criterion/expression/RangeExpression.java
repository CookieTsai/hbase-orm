package tw.cookie.orm.query.criterion.expression;

import tw.cookie.orm.query.criterion.Operator;

public class RangeExpression extends Expression {
    private final Object from;
    private final Object to;

    public RangeExpression(Operator operator, String propertyName, Object from, Object to) {
        super(operator, propertyName);
        this.from = from;
        this.to = to;
    }

    public Object getFrom() {
        return from;
    }

    public Object getTo() {
        return to;
    }
}
