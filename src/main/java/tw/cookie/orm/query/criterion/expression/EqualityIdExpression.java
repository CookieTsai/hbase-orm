package tw.cookie.orm.query.criterion.expression;

import tw.cookie.orm.query.criterion.Operator;

public class EqualityIdExpression extends Expression {

    private final Object value;

    public EqualityIdExpression(Operator operator, Object value) {
        super(operator, null);
        this.value = value;
    }

    public Object getValue() {
        return value;
    }
}
