package tw.cookie.orm.query.criterion.junction;

import tw.cookie.orm.query.criterion.Criterion;
import tw.cookie.orm.query.criterion.Operator;
import tw.cookie.orm.query.criterion.WithOperator;

import java.util.ArrayList;
import java.util.List;

public class Junction implements Criterion, WithOperator {
    private final List<Criterion> criteria = new ArrayList<Criterion>();
    private final Operator operator;

    protected Junction(Operator operator) {
        this.operator = operator;
    }

    public Junction add(Criterion criterion) {
        if (criterion != null) {
            criteria.add(criterion);
        }
        return this;
    }

    public Junction addAll(Criterion... criterions) {
        for (Criterion criterion : criterions) {
            add(criterion);
        }
        return this;
    }

    @Override
    public Operator getOperator() {
        return operator;
    }

    public List<Criterion> getCriteria() {
        // TODO : immutable list?
        return new ArrayList<Criterion>(criteria);
    }
}
