package tw.cookie.orm.query.criterion;

public interface WithOperator {

    Operator getOperator();

}
