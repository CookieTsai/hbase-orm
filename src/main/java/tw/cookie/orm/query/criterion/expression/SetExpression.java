package tw.cookie.orm.query.criterion.expression;

import tw.cookie.orm.query.criterion.Operator;

public class SetExpression extends Expression {

    private final Object[] values;

    public SetExpression(Operator operator, String propertyName, final Object[] values) {
        super(operator, propertyName);
        this.values = values;
    }

    public Object[] getValues() {
        return values;
    }

}
