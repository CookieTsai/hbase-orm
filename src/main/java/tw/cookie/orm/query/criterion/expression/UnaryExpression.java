package tw.cookie.orm.query.criterion.expression;

import tw.cookie.orm.query.criterion.Operator;

public class UnaryExpression extends Expression {

    public UnaryExpression(Operator operator, String propertyName) {
        super(operator, propertyName);
    }

}
