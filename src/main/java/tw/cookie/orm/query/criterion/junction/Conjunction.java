package tw.cookie.orm.query.criterion.junction;

import tw.cookie.orm.query.criterion.Criterion;
import tw.cookie.orm.query.criterion.Operator;

public class Conjunction extends Junction {

    public Conjunction() {
        super(Operator.AND);
    }

    public Conjunction(Criterion... criteria) {
        this();
        addAll(criteria);
    }
}
