package tw.cookie.orm.query.criterion;

public class Selector {

    public static final String NULL = "null";
    public static final String EMPTY = "";

    public static <T> T ifNull(T value, T defaultValue) {
        if (value == null || (value.getClass() == String.class && value.equals(NULL))) {
            return defaultValue;
        }
        return value;
    }

    public static <T> T ifEmpty(T value, T defaultValue) {
        if (value == null || (value.getClass() == String.class && (value.equals(EMPTY) || value.equals(NULL)))) {
            return defaultValue;
        }
        return value;
    }
}
