package tw.cookie.orm.query.criterion;

public enum  Operator {
    EQUAL("="), NOT_EQUAL("!="), GREATER_THAN(">"), GREATER_THAN_OR_EQUAL(">="), LESS_THAN("<"), LESS_THAN_OR_EQUAL("<="),

    BETWEEN("between"),

//    LIKE("like"), ILIKE("ilike"),

    NULL("null"), NOT_NULL("not null"), EMPTY("empty"), NOT_EMPTY("not empty"),

    IN("in"), NOT_IN("not in"),

    AND("and"), OR("or"),

    SUB("sub"), REGEX("regex");

    private final String symbol;

    Operator(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
