package tw.cookie.orm.query.criterion.junction;

import tw.cookie.orm.query.criterion.Criterion;
import tw.cookie.orm.query.criterion.Operator;

public class Disjunction extends Junction {

    public Disjunction() {
        super(Operator.OR);
    }

    public Disjunction(Criterion... criteria) {
        this();
        addAll(criteria);
    }
}
