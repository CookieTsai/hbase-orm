package tw.cookie.orm.query.criterion;

import tw.cookie.orm.query.criterion.expression.*;
import tw.cookie.orm.query.criterion.junction.Conjunction;
import tw.cookie.orm.query.criterion.junction.Disjunction;

import java.util.Collection;

public class Restrictions {
    private Restrictions() {}

    /**
     * Apply an "equal" constraint to the named property
     *
     * @param column
     *            String
     * @param value
     *            Object
     * @return Criterion
     */
    public static EqualityExpression eq(String column, Object value) {
        return new EqualityExpression(Operator.EQUAL, column, value);
    }

    /**
     * Apply a "not equal" constraint to the named property
     *
     * @param column
     *            String
     * @param value
     *            Object
     * @return Criterion
     */
    public static EqualityExpression ne(String column, Object value) {
        return new EqualityExpression(Operator.NOT_EQUAL, column, value);
    }

    /**
     * Apply a "less than" constraint to the named property
     *
     * @param column
     *            String
     * @param value
     *            Object
     * @return Criterion
     */
    public static EqualityExpression lt(String column, Object value) {
        return new EqualityExpression(Operator.LESS_THAN, column, value);
    }

    /**
     * Apply a "less than or equal" constraint to the named property
     *
     * @param column
     *            String
     * @param value
     *            Object
     * @return Criterion
     */
    public static EqualityExpression lte(String column, Object value) {
        return new EqualityExpression(Operator.LESS_THAN_OR_EQUAL, column, value);
    }

    /**
     * Apply a "greater than" constraint to the named property
     *
     * @param column
     *            String
     * @param value
     *            Object
     * @return Criterion
     */
    public static EqualityExpression gt(String column, Object value) {
        return new EqualityExpression(Operator.GREATER_THAN, column, value);
    }

    /**
     * Apply a "greater than or equal" constraint to the named property
     *
     * @param column
     *            String
     * @param value
     *            Object
     * @return Criterion
     */
    public static EqualityExpression gte(String column, Object value) {
        return new EqualityExpression(Operator.GREATER_THAN_OR_EQUAL, column, value);
    }

    public static EqualityExpression substring(String column, Object value) {
        return new EqualityExpression(Operator.SUB, column, value);
    }

    public static EqualityExpression regexString(String column, Object value) {
        return new EqualityExpression(Operator.REGEX, column, value);
    }

    public static EqualityIdExpression substringId(Object value) {
        return new EqualityIdExpression(Operator.SUB, value);
    }

    public static EqualityIdExpression regexStringId(Object value) {
        return new EqualityIdExpression(Operator.REGEX, value);
    }

    /**
     * Apply a "between" constraint to the named property
     *
     * @param column
     *            String
     * @param from
     *            Object
     * @param to
     *            Object
     * @return Criterion
     */
    public static RangeExpression between(String column, Object from, Object to) {
        return new RangeExpression(Operator.BETWEEN, column, from, to);
    }

    /**
     * Apply an "in" constraint to the named property
     *
     * @param column
     *            String
     * @param values
     *            Object[]
     * @return Criterion
     */
    public static SetExpression in(String column, Object... values) {
        return new SetExpression(Operator.IN, column, values);
    }

    /**
     * Apply an "in" constraint to the named property
     *
     * @param column
     *            String
     * @param values
     *            Collection
     * @return Criterion
     */
    public static SetExpression in(String column, Collection<? extends Object> values) {
        return in(column, values.toArray());
    }

    /**
     * Apply a "not in" constraint to the named property
     *
     * @param column
     *            String
     * @param values
     *            Object[]
     * @return Criterion
     */
    public static SetExpression notIn(String column, Object... values) {
        return new SetExpression(Operator.NOT_IN, column, values);
    }

    /**
     * Apply a "not in" constraint to the named property
     *
     * @param column
     *            String
     * @param values
     *            Collection
     * @return Criterion
     */
    public static SetExpression notIn(String column, Collection<? extends Object> values) {
        return notIn(column, values.toArray());
    }

    /**
     * Apply an "is null" constraint to the named property
     *
     * @param column
     *            String
     * @return Criterion
     */
    public static UnaryExpression isNull(String column) {
        return new UnaryExpression(Operator.NULL, column);
    }

    /**
     * Apply an "is not null" constraint to the named property
     *
     * @param column
     *            String
     * @return Criterion
     */
    public static UnaryExpression isNotNull(String column) {
        return new UnaryExpression(Operator.NOT_NULL, column);
    }

    /**
     * Constrain a collection valued property to be empty
     *
     * @param column
     *            String
     * @return UnaryExpression
     *
     */
    public static UnaryExpression isEmpty(String column) {
        return new UnaryExpression(Operator.EMPTY, column);
    }

    public static UnaryExpression isNotEmpty(String column) {
        return new UnaryExpression(Operator.NOT_EMPTY, column);
    }

    /**
     * Return the conjunction of two expressions
     *
     * @param criteria
     *            Criterion
     * @return Conjunction
     */
    public static Conjunction and(Criterion... criteria) {
        return new Conjunction(criteria);
    }

    /**
     * Return the disjunction of two expressions
     *
     * @param criteria
     *            Criterion
     * @return Disjunction
     */
    public static Disjunction or(Criterion... criteria) {
        return new Disjunction(criteria);
    }
}
