package tw.cookie.orm.query.criterion.expression;

import tw.cookie.orm.query.criterion.Operator;

public class EqualityExpression extends Expression {

    private final Object value;

    public EqualityExpression(Operator operator, String propertyName, Object value) {
        super(operator, propertyName);
        this.value = value;
    }

    public Object getValue() {
        return value;
    }
}
