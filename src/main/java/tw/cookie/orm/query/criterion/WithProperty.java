package tw.cookie.orm.query.criterion;

public interface WithProperty {

    String getPropertyName();

}
