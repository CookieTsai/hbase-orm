package tw.cookie.orm.query;

import tw.cookie.orm.translator.Translator;

import java.util.Iterator;

public abstract class AbstractQueryResult<T, E> implements QueryResult<E> {

    private final Iterator<T> iterator;
    private final Translator<T, E> translator;

    public AbstractQueryResult(Iterator<T> iterator, Translator<T, E> translator) {
        this.iterator = iterator;
        this.translator = translator;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public E next() {
        return translator.translate(iterator.next());
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<E> iterator() {
        return this;
    }
}
