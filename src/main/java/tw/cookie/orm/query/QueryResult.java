package tw.cookie.orm.query;

import java.util.Iterator;

public interface QueryResult<E> extends Iterator<E>, Iterable<E> {
}
