package tw.cookie.orm.query;

import tw.cookie.orm.query.criterion.Criterion;

public abstract class AbstractQuery implements Query {

    private final Criterion criteria;

    public AbstractQuery(Criterion criteria) {
        this.criteria = criteria;
    }

    @Override
    public Criterion getCriteria() {
        return criteria;
    }
}
