package tw.cookie.orm.coder.hbase;

import org.apache.hadoop.hbase.util.Bytes;

public class IntegerCoder extends AbstractHBaseCoder<Integer> {

    @Override
    protected byte[] encodeHBase(Integer integer) {
        return Bytes.toBytes(integer);
    }

    @Override
    protected Integer decodeHBase(byte[] bytes) {
        return Bytes.toInt(bytes);
    }
}
