package tw.cookie.orm.coder.hbase;

import org.apache.hadoop.hbase.util.Bytes;

public class DoubleCoder extends AbstractHBaseCoder<Double> {

    @Override
    protected byte[] encodeHBase(Double aDouble) {
        return Bytes.toBytes(aDouble);
    }

    @Override
    protected Double decodeHBase(byte[] bytes) {
        return Bytes.toDouble(bytes);
    }
}
