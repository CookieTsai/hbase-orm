package tw.cookie.orm.coder.hbase;

import tw.cookie.orm.coder.Coder;

public abstract class AbstractHBaseCoder<T> implements Coder<Object, byte[]> {

    @Override
    @SuppressWarnings("unchecked")
    public byte[] encode(Object object) {
        if (object == null) {
            return null;
        }
        return encodeHBase((T) object);
    }

    protected abstract byte[] encodeHBase(T t);

    @Override
    public Object decode(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        return decodeHBase(bytes);
    }

    protected abstract T decodeHBase(byte[] bytes);
}