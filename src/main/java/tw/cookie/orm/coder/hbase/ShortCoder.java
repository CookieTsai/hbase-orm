package tw.cookie.orm.coder.hbase;

import org.apache.hadoop.hbase.util.Bytes;

public class ShortCoder extends AbstractHBaseCoder<Short> {

    @Override
    protected byte[] encodeHBase(Short aShort) {
        return Bytes.toBytes(aShort);
    }

    @Override
    protected Short decodeHBase(byte[] bytes) {
        return Bytes.toShort(bytes);
    }
}
