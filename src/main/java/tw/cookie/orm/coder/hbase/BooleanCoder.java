package tw.cookie.orm.coder.hbase;

import org.apache.hadoop.hbase.util.Bytes;

public class BooleanCoder extends AbstractHBaseCoder<Boolean> {
    @Override
    protected byte[] encodeHBase(Boolean aBoolean) {
        return Bytes.toBytes(aBoolean);
    }

    @Override
    protected Boolean decodeHBase(byte[] bytes) {
        return Bytes.toBoolean(bytes);
    }
}
