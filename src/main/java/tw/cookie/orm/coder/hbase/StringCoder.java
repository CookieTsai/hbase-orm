package tw.cookie.orm.coder.hbase;

import org.apache.hadoop.hbase.util.Bytes;

public class StringCoder extends AbstractHBaseCoder<String> {

    @Override
    protected byte[] encodeHBase(String s) {
        return Bytes.toBytes(s);
    }

    @Override
    protected String decodeHBase(byte[] bytes) {
        return Bytes.toString(bytes);
    }
}
