package tw.cookie.orm.coder.hbase;

import org.apache.hadoop.hbase.util.Bytes;

public class LongCoder extends AbstractHBaseCoder<Long> {

    @Override
    protected byte[] encodeHBase(Long aLong) {
        return Bytes.toBytes(aLong);
    }

    @Override
    protected Long decodeHBase(byte[] bytes) {
        return Bytes.toLong(bytes);
    }
}
