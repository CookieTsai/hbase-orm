package tw.cookie.orm.coder.hbase;

import org.apache.hadoop.hbase.util.Bytes;

public class FloatCoder extends AbstractHBaseCoder<Float> {

    @Override
    protected byte[] encodeHBase(Float aFloat) {
        return Bytes.toBytes(aFloat);
    }

    @Override
    protected Float decodeHBase(byte[] bytes) {
        return Bytes.toFloat(bytes);
    }
}
