package tw.cookie.orm.coder.hbase;

public class BytesCoder extends AbstractHBaseCoder<byte[]> {

    @Override
    protected byte[] encodeHBase(byte[] bytes) {
        return bytes;
    }

    @Override
    protected byte[] decodeHBase(byte[] bytes) {
        return bytes;
    }
}
