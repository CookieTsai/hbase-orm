package tw.cookie.orm.coder;

import tw.cookie.orm.coder.hbase.*;

import java.util.HashMap;
import java.util.Map;

public class HBaseCoderMapper {

    private static final Map<Class<?>, AbstractHBaseCoder<?>> coderMap = new HashMap<>();

    static {
        put(new BytesCoder(),   Byte[].class,   byte[].class);
        put(new StringCoder(),  String.class);
        put(new LongCoder(),    Long.class,     long.class);
        put(new IntegerCoder(), Integer.class,  int.class);
        put(new BooleanCoder(), Boolean.class,  boolean.class);
        put(new DoubleCoder(),  Double.class,   double.class);
        put(new FloatCoder(),   Float.class,    float.class);
        put(new ShortCoder(),   Short.class,    short.class);
    }

    private HBaseCoderMapper() {}

    private static void put(AbstractHBaseCoder<?> coder, Class<?>... classes) {
        for (Class<?> clazz: classes) {
            coderMap.put(clazz, coder);
        }
    }

    public static AbstractHBaseCoder<?> getCoder(Class<?> type) {
        AbstractHBaseCoder<?> coder = coderMap.get(type);
        if (coder == null) {
            throw new UnsupportedOperationException(type.getSimpleName() + " type not supported.");
        }
        return coder;
    }
}
