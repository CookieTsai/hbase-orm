package tw.cookie.orm.coder;

public interface Coder<PLAINTEXT, CIPHER> {
    CIPHER encode(Object object);
    PLAINTEXT decode(CIPHER cipher);
}
