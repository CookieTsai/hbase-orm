package tw.cookie.orm;

import tw.cookie.orm.query.AbstractQueryResult;
import tw.cookie.orm.translator.ResultTranslator;
import org.apache.hadoop.hbase.client.Result;

import java.util.Iterator;

public class HBaseQueryResult<E> extends AbstractQueryResult<Result, E> {
    public HBaseQueryResult(Class<E> entityType, Iterator<Result> iterator) {
        super(iterator, new ResultTranslator<>(entityType));
    }
}
