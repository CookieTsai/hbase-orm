package tw.cookie.orm.closer;

public interface Closer<T> {
    Closer<T> add(T t);
    boolean close();
}
