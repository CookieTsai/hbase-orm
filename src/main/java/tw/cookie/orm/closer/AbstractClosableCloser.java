package tw.cookie.orm.closer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractClosableCloser implements Closer<Closeable> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractClosableCloser.class);

    private List<Closeable> closeables = new ArrayList<>();

    @Override
    public Closer<Closeable> add(Closeable closeable) {
        closeables.add(closeable);
        return this;
    }

    @Override
    public boolean close() {
        for (Closeable closeable: closeables) {
            try {
                if (closeable != null) {
                    closeable.close();
                }
            } catch (Throwable throwable) {
                LOGGER.warn(String.format("Closer close %s with Exception", closeable.getClass()), throwable);
            }
        }
        closeables = null;
        return false;
    }
}
