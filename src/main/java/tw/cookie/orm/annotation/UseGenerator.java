package tw.cookie.orm.annotation;

import tw.cookie.orm.generator.GenType;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
public @interface UseGenerator {
    GenType genType() default GenType.SHA_ROW;
}