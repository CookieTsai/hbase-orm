package tw.cookie.orm.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Target(TYPE)
@Retention(RUNTIME)
public @interface Entity {
    String CF = "cf";
    /**
     * (Optional) The name of the entity.
     * <p/>
     * Defaults to the entity class default name.
     */
    String name() default "";

    String cf() default CF;
}
