package tw.cookie.orm.register;

public interface Register<K, T> {
    Register register(T t);
    boolean isRegister(T t);
    K getKey(T t);
}
