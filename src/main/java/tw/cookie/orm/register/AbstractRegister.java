package tw.cookie.orm.register;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractRegister<K, T> implements Register<K, T> {
    private volatile Set<T> set = new HashSet<>();

    @Override
    public Register register(T t) {
        set.add(t);
        return this;
    }

    @Override
    public boolean isRegister(T t) {
        return set.contains(t);
    }
}
