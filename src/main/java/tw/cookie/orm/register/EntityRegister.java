package tw.cookie.orm.register;

import tw.cookie.orm.annotation.Entity;

public class EntityRegister extends AbstractRegister<String, Class<?>> {

    private static final EntityRegister INSTANCE = new EntityRegister();

    private EntityRegister() {}

    @Override
    public synchronized Register register(Class<?> type) {
        if (super.isRegister(type)) {
            throw new RuntimeException(String.format("%s has been register", type));
        }
        return super.register(type);
    }

    @Override
    public String getKey(Class<?> type) {
        return getEntityName(type);
    }

    private <R> String getEntityName(Class<R> type) {
        Entity entity = type.getAnnotation(Entity.class);
        return entity == null || entity.name().equals("")? type.getSimpleName() : entity.name();
    }

    public static EntityRegister getInstance() {
        return INSTANCE;
    }
}
