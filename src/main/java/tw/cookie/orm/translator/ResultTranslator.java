package tw.cookie.orm.translator;

import tw.cookie.orm.annotation.Column;
import tw.cookie.orm.annotation.EmbeddedId;
import tw.cookie.orm.annotation.Entity;
import tw.cookie.orm.coder.HBaseCoderMapper;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import java.lang.reflect.Field;

public class ResultTranslator<R> implements Translator<Result, R> {

    private final Class<R> returnType;

    public ResultTranslator(Class<R> returnType) {
        this.returnType = returnType;
    }

    @Override
    public R translate(Result result) {
        try {
            if (result == null || result.isEmpty()) {
                return null;
            }

            Entity e = returnType.getAnnotation(Entity.class);
            String cfName = (e == null || e.cf().equals(""))? Entity.CF : e.cf();

            return build(result, cfName, returnType);
        } catch (Throwable t) {
            throw new RuntimeException("ResultTranslator.translate(Result result) is Failed", t);
        }
    }

    private <T> T build(Result result, String cfName, Class<T> type) throws IllegalAccessException, InstantiationException {
        T instance = type.newInstance();

        for (Field f: type.getDeclaredFields()) {
            f.setAccessible(true);

            EmbeddedId embeddedId = f.getAnnotation(EmbeddedId.class);
            if (embeddedId != null) {
                f.set(instance, build(result, cfName, f.getType()));
            } else {
                Column column = f.getAnnotation(Column.class);
                String cqName = (column == null || column.name().equals(""))? f.getName() : column.name();

                Cell cell = result.getColumnLatestCell(Bytes.toBytes(cfName), Bytes.toBytes(cqName));

                if (cell == null) continue;

                byte[] bytes = CellUtil.cloneValue(cell);
                f.set(instance, HBaseCoderMapper.getCoder(f.getType()).decode(bytes));
            }
        }

        return instance;
    }
}
