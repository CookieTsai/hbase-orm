package tw.cookie.orm.translator;

/**
 * Description of a class that translates a generic Query to a datastore specific query
 *
 * @param <IN>
 *            INPUT Type
 * @param <OUT>
 *            OUTPUT Type
 */
public interface Translator<IN, OUT> {

    /**
     * Translate the generic Query
     *
     * @param in
     *            INPUT Object
     * @return OUT
     *            OUTPUT Object
     */
    OUT translate(IN in);
}
