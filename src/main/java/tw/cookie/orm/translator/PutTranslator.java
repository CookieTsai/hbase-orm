package tw.cookie.orm.translator;

import tw.cookie.orm.annotation.*;
import tw.cookie.orm.coder.HBaseCoderMapper;
import tw.cookie.orm.register.EntityRegister;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

import java.lang.reflect.Field;

public class PutTranslator implements Translator<Object, Put> {

    private static final Translator<Object, String> rowKeyTranslator = new RowKeyObjTranslator();

    @Override
    public Put translate(Object o) {
        try {
            Put result = new Put(Bytes.toBytes(getRowKey(o)));

            Entity e = o.getClass().getAnnotation(Entity.class);
            String cfName = (e == null || e.cf().equals(""))? Entity.CF : e.cf();

            result.addColumn(
                    Bytes.toBytes(cfName),
                    Bytes.toBytes("!entity"),
                    Bytes.toBytes(EntityRegister.getInstance().getKey(o.getClass())));

            return putColumn(result, cfName, o);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("translate(Object o) is Failed", e);
        }
    }

    private Put putColumn(Put result, String cfName, Object o) throws IllegalAccessException {
        for (Field f: o.getClass().getDeclaredFields()) {
            f.setAccessible(true);

            // Set cqName
            Column column = f.getAnnotation(Column.class);
            String cqName = (column == null || column.name().equals(""))? f.getName() : column.name();

            // Set Value
            Object value = f.get(o);

            if (column != null && !column.nullable() && value == null) {
                throw new RuntimeException(String.format("%s's %s value should not Null", o.getClass(), cqName));
            }

            EmbeddedId embeddedId = f.getAnnotation(EmbeddedId.class);
            if (embeddedId == null) {
                if (value == null) continue;
                result.addColumn(Bytes.toBytes(cfName), Bytes.toBytes(cqName), HBaseCoderMapper.getCoder(value.getClass()).encode(value));
            } else {
                putColumn(result, cfName, f.get(o));
            }
        }

        return result;
    }

    private String getRowKey(Object o) throws IllegalAccessException {
        String result = null;

        for (Field f: o.getClass().getDeclaredFields()) {
            f.setAccessible(true);

            Id id = f.getAnnotation(Id.class);
            if (id != null) {
                String rowKey = (String) f.get(o);
                UseGenerator generator = f.getAnnotation(UseGenerator.class);
                result = (generator == null)? id.genType().gen(rowKey) : generator.genType().gen(rowKey);
                break;
            }

            EmbeddedId embeddedId = f.getAnnotation(EmbeddedId.class);
            if (embeddedId != null) {
                result = rowKeyTranslator.translate(f.get(o));
                break;
            }
        }
        if (result == null) {
            throw new RuntimeException(String.format(" %s's Id Can't Not be Found.", o.getClass().getSimpleName()));
        }
        return result;
    }
}
