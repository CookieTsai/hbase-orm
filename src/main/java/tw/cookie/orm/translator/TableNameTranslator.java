package tw.cookie.orm.translator;

import tw.cookie.orm.annotation.Table;

public class TableNameTranslator implements Translator<Object, String> {
    @Override
    public String translate(Object o) {
        Table table = o.getClass().getAnnotation(Table.class);
        if (table == null) {
            throw new RuntimeException("Can't Not Translate Table Name");
        }
        return table.name();
    }
}
