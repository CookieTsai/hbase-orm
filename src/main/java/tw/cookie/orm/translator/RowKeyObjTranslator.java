package tw.cookie.orm.translator;

import tw.cookie.orm.annotation.Embeddable;
import tw.cookie.orm.annotation.UseGenerator;

import java.lang.reflect.Field;

public class RowKeyObjTranslator implements Translator<Object, String> {

    @Override
    public String translate(Object o) {
        try {
            if (o == null) {
                return null;
            } else if (o.getClass() == String.class) {
                return (String) o;
            }

            Class<?> type = o.getClass();

            Embeddable embeddable = type.getAnnotation(Embeddable.class);
            if (embeddable == null) {
                throw new UnsupportedOperationException(type.getSimpleName() + " type not supported.");
            }

            StringBuilder builder = new StringBuilder();
            for (Field field: type.getDeclaredFields()) {
                field.setAccessible(true);

                String subRowKey = (String) field.get(o);
                UseGenerator generator = field.getAnnotation(UseGenerator.class);
                subRowKey = (generator == null)? subRowKey : generator.genType().gen(subRowKey);

                if (builder.length() > 0) {
                    builder.append("_");
                }
                builder.append(subRowKey);
            }
            String result = builder.toString();

            if (result.isEmpty()) {
                throw new RuntimeException("result is Empty");
            }

            return result;
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Can't Not Translate Id", e);
        }
    }
}
