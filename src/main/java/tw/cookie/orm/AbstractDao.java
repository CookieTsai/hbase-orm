package tw.cookie.orm;

import tw.cookie.orm.generator.Generator;
import tw.cookie.orm.generator.Sha64Generator;
import tw.cookie.orm.generator.ShaRowGenerator;
import tw.cookie.orm.query.Query;
import tw.cookie.orm.query.QueryResult;
import tw.cookie.orm.query.QueryResults;
import tw.cookie.orm.translator.TableNameTranslator;
import tw.cookie.orm.translator.Translator;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDao<T extends Executor> implements Dao<T>, Closeable {

    private static final Translator<Object, String> tableNameTranslator = new TableNameTranslator();

    private static final Generator<String, String> shaRowGenerator      = new ShaRowGenerator();
    private static final Generator<String, String> sha64Generator       = new Sha64Generator();

    private final Executor executor;

    {
        try {
            Type[] types = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments();
            executor = (Executor) ((Class<?>) types[0]).newInstance();
        } catch (Throwable t) {
            throw new RuntimeException("AbstractDao init Failed");
        }
    }

    @Override
    public <E> E save(E entity) {
        return executor.save(getTableName(), entity);
    }

    @Override
    public <E> Iterable<E> save(Iterable<E> entities) {
        return executor.save(getTableName(), entities);
    }

    @Override
    public <E, RK> E getById(RK rk, Class<E> returnType) {
        return executor.getById(getTableName(), rk, returnType);
    }

    public <E> E getByShaRowId(String rk, Class<E> returnType) {
        return getById(shaRowGenerator.gen(rk), returnType);
    }

    public <E> E getBySha64Id(String rk, Class<E> returnType) {
        return getById(sha64Generator.gen(rk), returnType);
    }

    @Override
    public <RK> boolean delete(RK rk) {
        return executor.delete(getTableName(), rk);
    }

    @Override
    public <RK> boolean delete(Iterable<RK> rks) {
        return executor.delete(getTableName(), rks);
    }

    public boolean deleteShaRow(Iterable<String> rks) {
        List<String> shaRowList = new ArrayList<>();
        for (String rk: rks) {
            shaRowList.add(shaRowGenerator.gen(rk));
        }
        return delete(shaRowList);
    }

    public boolean deleteSha64(Iterable<String> rks) {
        List<String> shaRowList = new ArrayList<>();
        for (String rk: rks) {
            shaRowList.add(sha64Generator.gen(rk));
        }
        return delete(shaRowList);
    }

    public boolean deleteShaRow(String rk) {
        return delete(shaRowGenerator.gen(rk));
    }

    public boolean deleteSha64(String rk) {
        return delete(sha64Generator.gen(rk));
    }

    @Override
    public <E> QueryResult<E> find(Query query, Class<E> returnType) {
        return executor.find(getTableName(), query, returnType);
    }

    @Override
    public QueryResults find(Query query) {
        return executor.find(getTableName(), query);
    }

    @Override
    public <E> E findOne(Class<E> returnType, Query query) {
        return executor.findOne(getTableName(), returnType, query);
    }

    @Override
    public void close() throws IOException {
        executor.close();
    }

    private String getTableName() {
        return tableNameTranslator.translate(this);
    }
}
