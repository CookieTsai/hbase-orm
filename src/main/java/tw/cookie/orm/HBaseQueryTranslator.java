package tw.cookie.orm;

import tw.cookie.orm.annotation.Entity;
import tw.cookie.orm.coder.HBaseCoderMapper;
import tw.cookie.orm.query.AbstractQueryTranslator;
import tw.cookie.orm.query.criterion.Selector;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.hadoop.hbase.filter.CompareFilter.CompareOp.*;
import static org.apache.hadoop.hbase.filter.FilterList.Operator.*;

public class HBaseQueryTranslator extends AbstractQueryTranslator<HBaseQuery, Scan, Filter> {
    private static final Logger LOGGER = LoggerFactory.getLogger(HBaseQueryTranslator.class);

    private static final byte[] NULL = Bytes.toBytes(Selector.NULL);
    private static final byte[] EMPTY = Bytes.toBytes(Selector.EMPTY);

    private static final String SPLIT_STR = ":";
    private static final String DEFAULT_ENTITY_CF = Entity.CF;
    private static final int DEFAULT_CACHING = 50;

    public HBaseQueryTranslator() {
        super(HBaseQuery.class, Scan.class, Filter.class);
    }

    @Override
    public Scan translate(HBaseQuery query) {
        try {
            Scan scan = new Scan();

            if (query.getCriteria() != null) {
                Filter filter = translate(query.getCriteria());
                if (filter != null) {
                    scan.setFilter(filter);
                }
            }

            if (query.getStartRow() != null) {
                scan.setStartRow(Bytes.toBytes(query.getStartRow()));
            }

            if (query.getStopRow() != null) {
                scan.setStartRow(Bytes.toBytes(query.getStopRow()));
            }

            if (query.getMinTime() > 0L || query.getMaxTime() < Long.MAX_VALUE) {
                scan.setTimeRange(query.getMinTime(), query.getMaxTime());
            }

            if (query.getLimit() != null && query.getLimit() > 0) {
                scan.setMaxResultSize(query.getLimit());
                scan.setCaching(query.getLimit());
            } else if (query.getCaching() > 0) {
                scan.setCaching(query.getCaching());
            } else {
                scan.setCaching(DEFAULT_CACHING);
            }

            if (query.getBatch() > 0) {
                scan.setBatch(query.getBatch());
            }

            if (query.getReversed() != null) {
                scan.setReversed(query.getReversed());
            }

            return scan;
        } catch (Throwable throwable) {
            throw new RuntimeException("HBaseQueryTranslator.translate(HBaseQuery query) is failed", throwable);
        }
    }

    @Override
    public Filter eq(String fieldName, Object value) {
        return newSingleColumnValueFilter(fieldName, EQUAL, value, true);
    }

    @Override
    public Filter ne(String fieldName, Object value) {
        return newSingleColumnValueFilter(fieldName, NOT_EQUAL, value, true);
    }

    @Override
    public Filter lt(String fieldName, Object value) {
        return newSingleColumnValueFilter(fieldName, LESS, value, true);
    }

    @Override
    public Filter lte(String fieldName, Object value) {
        return newSingleColumnValueFilter(fieldName, LESS_OR_EQUAL, value, true);
    }

    @Override
    public Filter gt(String fieldName, Object value) {
        return newSingleColumnValueFilter(fieldName, GREATER, value, true);
    }

    @Override
    public Filter gte(String fieldName, Object value) {
        return newSingleColumnValueFilter(fieldName, GREATER_OR_EQUAL, value, true);
    }

    @Override
    public Filter between(String fieldName, Object from, Object to) {
        return and(gte(fieldName, from), lte(fieldName, to));
    }

    @Override
    public Filter in(String fieldName, Object[] values) {
        FilterList filterList = new FilterList(MUST_PASS_ONE);
        for (Object value: values) {
            filterList.addFilter(eq(fieldName, value));
        }
        return filterList;
    }

    @Override
    public Filter notIn(String fieldName, Object[] values) {
        FilterList filterList = new FilterList(MUST_PASS_ALL);
        for (Object value: values) {
            filterList.addFilter(ne(fieldName, value));
        }
        return filterList;
    }

    @Override
    public Filter isNull(String fieldName) {
        return newSingleColumnValueFilter(fieldName, EQUAL, NULL, false);
    }

    @Override
    public Filter notNull(String fieldName) {
        return ne(fieldName, NULL);
    }

    @Override
    public Filter isEmpty(String fieldName) {
        return or(isNull(fieldName), eq(fieldName, EMPTY));
    }

    @Override
    public Filter notEmpty(String fieldName) {
        return and(notNull(fieldName), ne(fieldName, EMPTY));
    }

    @Override
    protected Filter regexString(String fieldName, Object value) {
        return newSingleColumnValueFilter(fieldName, EQUAL, new RegexStringComparator((String) value), true);
    }

    @Override
    protected Filter substring(String fieldName, Object value) {
        return newSingleColumnValueFilter(fieldName, EQUAL, new SubstringComparator((String) value), true);
    }

    @Override
    protected Filter regexStringId(String fieldName, Object value) {
        return newRowFilter(EQUAL, new RegexStringComparator((String) value));
    }

    @Override
    protected Filter substringId(String fieldName, Object value) {
        return newRowFilter(EQUAL, new SubstringComparator((String) value));
    }

    @Override
    public Filter and(Filter... filters) {
        FilterList filterList = new FilterList(MUST_PASS_ALL);
        for (Filter filter: filters) {
            filterList.addFilter(filter);
        }
        return filterList;
    }

    @Override
    public Filter or(Filter... filters) {
        FilterList filterList = new FilterList(MUST_PASS_ONE);
        for (Filter filter: filters) {
            filterList.addFilter(filter);
        }
        return filterList;
    }

    private SingleColumnValueFilter newSingleColumnValueFilter(String fieldName, CompareFilter.CompareOp op, Object value, boolean ifMissing) {
        String[] parts = fieldName.split(SPLIT_STR);
        String cf, cq;
        if (parts.length == 1) {
            cf = DEFAULT_ENTITY_CF;
            cq = parts[0];
        } else if (parts.length == 2) {
            cf = parts[0];
            cq = parts[1];
        } else {
            throw new RuntimeException("fieldName is not Valid !!");
        }
        ByteArrayComparable comparable;
        if (value == null) {
            comparable = new BinaryComparator(null);
        } else if (value instanceof ByteArrayComparable) {
            comparable = (ByteArrayComparable) value;
        } else {
            comparable = new BinaryComparator(HBaseCoderMapper.getCoder(value.getClass()).encode(value));
        }
        SingleColumnValueFilter filter = new SingleColumnValueFilter(Bytes.toBytes(cf), Bytes.toBytes(cq), op, comparable);
        filter.setFilterIfMissing(ifMissing);
        return filter;
    }

    private RowFilter newRowFilter(CompareFilter.CompareOp op, Object value) {
        ByteArrayComparable comparable;
        if (value == null) {
            comparable = new BinaryComparator(null);
        } else if (value instanceof ByteArrayComparable) {
            comparable = (ByteArrayComparable) value;
        } else {
            comparable = new BinaryComparator(HBaseCoderMapper.getCoder(value.getClass()).encode(value));
        }
        return new RowFilter(op, comparable);
    }
}
