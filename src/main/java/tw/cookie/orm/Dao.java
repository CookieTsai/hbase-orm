package tw.cookie.orm;

import tw.cookie.orm.query.Query;
import tw.cookie.orm.query.QueryResult;
import tw.cookie.orm.query.QueryResults;

public interface Dao<T> {
    <E> E save(E entity);
    <E> Iterable<E> save(Iterable<E> entities);
    <E, RK> E getById(RK rk, Class<E> returnType);
    <RK> boolean delete(RK rk);
    <RK> boolean delete(Iterable<RK> rks);
    <E> QueryResult<E> find(Query query, Class<E> returnType);
    QueryResults find(Query query);
    <E> E findOne(Class<E> returnType, Query query);
}
