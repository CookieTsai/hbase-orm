package tw.cookie.orm;

import tw.cookie.orm.query.AbstractQuery;
import tw.cookie.orm.query.criterion.Criterion;

public class HBaseQuery extends AbstractQuery {

    private final String startRow;
    private final String stopRow;
    private final int caching;
    private final int batch;
    private final long minTime;
    private final long maxTime;
    private final Integer limit;
    private final Boolean reversed;

    public HBaseQuery(Criterion criteria, String startRow, String stopRow, int caching, int batch, long minTime, long maxTime, Integer limit, Boolean reversed) {
        super(criteria);
        this.startRow = startRow;
        this.stopRow = stopRow;
        this.caching = caching;
        this.batch = batch;
        this.minTime = minTime;
        this.maxTime = maxTime;
        this.limit = limit;
        this.reversed = reversed;
    }

    public String getStartRow() {
        return startRow;
    }

    public String getStopRow() {
        return stopRow;
    }

    public int getCaching() {
        return caching;
    }

    public int getBatch() {
        return batch;
    }

    public long getMinTime() {
        return minTime;
    }

    public long getMaxTime() {
        return maxTime;
    }

    public Boolean getReversed() {
        return reversed;
    }

    public Integer getLimit() {
        return limit;
    }
}
