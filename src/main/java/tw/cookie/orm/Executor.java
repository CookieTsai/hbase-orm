package tw.cookie.orm;

import tw.cookie.orm.query.Query;
import tw.cookie.orm.query.QueryResult;
import tw.cookie.orm.query.QueryResults;

import java.io.Closeable;

public interface Executor extends Closeable {
    <E> E save(String TableName, E entity);
    <E> Iterable<E> save(String tableName, Iterable<E> entities);
    <E, RK> E getById(String tableName, RK rk, Class<E> returnType);
    <RK> boolean delete(String tableName, RK rk);
    <RK> boolean delete(String tableName, Iterable<RK> rks);
    <E> QueryResult<E> find(String tableName, Query query, Class<E> returnType);
    QueryResults find(String tableName, Query query);
    <E> E findOne(String tableName, Class<E> returnType, Query query);
}
