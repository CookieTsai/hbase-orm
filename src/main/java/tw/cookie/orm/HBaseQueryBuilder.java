package tw.cookie.orm;

import tw.cookie.orm.annotation.Entity;
import tw.cookie.orm.query.QueryBuilder;
import tw.cookie.orm.query.criterion.Criterion;
import tw.cookie.orm.query.criterion.Restrictions;
import tw.cookie.orm.register.EntityRegister;

import java.util.ArrayList;
import java.util.List;

public class HBaseQueryBuilder implements QueryBuilder<HBaseQuery> {

    private List<Class<?>> entityTypes;
    private List<Criterion> criteria;

    private String startRow;
    private String stopRow;
    private int caching = -1;
    private int batch = -1;
    private long minTime = 0L;
    private long maxTime = Long.MAX_VALUE;
    private Integer limit;
    private Boolean reversed;

    private boolean useEntityType;

    public HBaseQueryBuilder() {
        this(true);
    }

    public HBaseQueryBuilder(boolean useEntityType) {
        this.entityTypes = new ArrayList<>();
        this.criteria = new ArrayList<>();
        this.useEntityType = useEntityType;
    }

    public HBaseQueryBuilder addEntity(Class<?> entityType) {
        entityTypes.add(entityType);
        return this;
    }

    public HBaseQueryBuilder add(Criterion criterion) {
        criteria.add(criterion);
        return this;
    }

    public HBaseQueryBuilder setStartRow(String startRow) {
        this.startRow = startRow;
        return this;
    }

    public HBaseQueryBuilder setStopRow(String stopRow) {
        this.stopRow = stopRow;
        return this;
    }

    public HBaseQueryBuilder setCaching(int caching) {
        this.caching = caching;
        return this;
    }

    public HBaseQueryBuilder setBatch(int batch) {
        this.batch = batch;
        return this;
    }


    public HBaseQueryBuilder setTimeRange(long minTime, long maxTime) {
        this.minTime = minTime;
        this.maxTime = maxTime;
        return this;
    }

    public HBaseQueryBuilder setLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public HBaseQueryBuilder setReversed(Boolean reversed) {
        this.reversed = reversed;
        return this;
    }

    private Criterion bindCriterion(List<Criterion> criteria, List<Class<?>> entityTypes) {
        Criterion criterion = null;

        if (useEntityType && entityTypes.size() <= 0) {
            throw new RuntimeException("Please Set The Entity Type");
        }

        // build entity type and put to criteria
        if (entityTypes.size() > 0) {
            criteria.add(build(entityTypes));
        }

        if (criteria.size() == 1) {
            criterion = criteria.get(0);
        } else if (criteria.size() > 1) {
            criterion = Restrictions.and(criteria.toArray(new Criterion[criteria.size()]));
        }
        return criterion;
    }

    private Criterion build(List<Class<?>> entityTypes) {
        EntityRegister register = EntityRegister.getInstance();

        List<Criterion> criteria = new ArrayList<>();
        for (Class<?> type: entityTypes) {
            if (!register.isRegister(type)) {
                throw new RuntimeException(String.format("The Entity %s has not be Register", type));
            }

            Entity e = type.getAnnotation(Entity.class);
            String cfName = (e == null || e.cf().equals(""))? Entity.CF : e.cf();

            criteria.add(Restrictions.eq(cfName + ":" + "!entity", register.getKey(type)));
        }
        return Restrictions.or(criteria.toArray(new Criterion[criteria.size()]));
    }

    @Override
    public HBaseQuery build() {
        Criterion rootCriterion = bindCriterion(criteria, entityTypes);
        return new HBaseQuery(rootCriterion, startRow, stopRow, caching, batch, minTime, maxTime, limit, reversed);
    }
}
