package tw.cookie.orm.generator;

/**
 * Description of a class that translates a generic Query to a datastore specific query
 *
 * @param <IN>
 *            INPUT Type
 * @param <OUT>
 *            OUTPUT Type
 */
public interface Generator<IN, OUT> {

    /**
     * Translate the generic Query
     *
     * @param in
     *            INPUT Objects
     * @return OUT
     *            OUTPUT Object
     */
    OUT gen(IN in);
}
