package tw.cookie.orm.generator;

import org.apache.commons.codec.binary.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ShaRowGenerator implements Generator<String, String> {

    private static final String ALGORITHM_SHA1 = "SHA1";
    private static final int PREFIX_LENGTH = 3;

    @Override
    public String gen(String text) {
        try {
            return Base64.encodeBase64String(sha1(text)).trim().substring(0, PREFIX_LENGTH) + text;
        } catch (Throwable throwable) {
            throw new RuntimeException("ShaRowGenerator generate Failed", throwable);
        }
    }

    private static byte[] sha1(String data) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance(ALGORITHM_SHA1);
        messageDigest.update(data.getBytes());
        return messageDigest.digest();
    }
}
