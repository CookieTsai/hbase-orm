package tw.cookie.orm.generator;

public enum GenType {
    SHA_ROW(ShaRowGenerator.class),
    SHA_64(Sha64Generator.class),
    NONE(NoneGenerator.class);

    private Generator<String, String> generator;

    GenType(Class<? extends Generator<String, String>> generatorClass) {
        try {
            this.generator = generatorClass.newInstance();
        } catch (Throwable throwable) {
            throw new RuntimeException("Generator Init Fail: " + generatorClass.getSimpleName());
        }
    }

    public String gen(String in) {
        return this.generator.gen(in);
    }
}
