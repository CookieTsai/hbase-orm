package tw.cookie.orm;

import tw.cookie.orm.closer.ClosableCloser;
import tw.cookie.orm.query.Query;
import tw.cookie.orm.query.QueryResult;
import tw.cookie.orm.query.QueryResults;
import tw.cookie.orm.register.EntityRegister;
import tw.cookie.orm.translator.PutTranslator;
import tw.cookie.orm.translator.ResultTranslator;
import tw.cookie.orm.translator.RowKeyObjTranslator;
import tw.cookie.orm.translator.Translator;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractHBaseExecutor implements Executor {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractHBaseExecutor.class);

    private static final EntityRegister entityRegister = EntityRegister.getInstance();

    private static final Translator<Object, Put> putTranslator = new PutTranslator();
    private static final Translator<Object, String> rowKeyTranslator = new RowKeyObjTranslator();
    private static final Translator<HBaseQuery, Scan> queryTranslator = new HBaseQueryTranslator();

    private final ClosableCloser closer = new ClosableCloser();

    private Table table;

    @Override
    public <E> E save(String tableName, E entity) {
        checkEntity(entity.getClass());
        try {
            getTable(tableName).put(putTranslator.translate(entity));
            return entity;
        } catch (Throwable t) {
            throw new RuntimeException("DAO Exception", t);
        }
    }

    @Override
    public <E> Iterable<E> save(String tableName, Iterable<E> entities) {
        try {
            List<Put> putList = new ArrayList<>();
            for (E e: entities) {
                checkEntity(e.getClass());
                putList.add(putTranslator.translate(e));
            }
            getTable(tableName).put(putList);
            return entities;
        } catch (Throwable t) {
            throw new RuntimeException("DAO Exception", t);
        }
    }

    @Override
    public <E, RK> E getById(String tableName, RK rk, Class<E> returnType) {
        try {
            checkEntity(returnType);
            String rowKey = rowKeyTranslator.translate(rk);
            Result result = getTable(tableName).get(new Get(Bytes.toBytes(rowKey)));
            return new ResultTranslator<>(returnType).translate(result);
        } catch (Throwable t) {
            throw new RuntimeException("DAO Exception", t);
        }
    }

    @Override
    public <RK> boolean delete(String tableName, RK rk) {
        try {
            String rowKey = rowKeyTranslator.translate(rk);
            getTable(tableName).delete(new Delete(Bytes.toBytes(rowKey)));
            return true;
        } catch (Throwable t) {
            throw new RuntimeException("DAO Exception", t);
        }
    }

    @Override
    public <RK> boolean delete(String tableName, Iterable<RK> rks) {
        try {
            List<Delete> deletes = new ArrayList<>();
            for (RK rk: rks) {
                String rowKey = rowKeyTranslator.translate(rk);
                deletes.add(new Delete(Bytes.toBytes(rowKey)));
            }
            getTable(tableName).delete(deletes);
            return true;
        } catch (Throwable t) {
            throw new RuntimeException("DAO Exception", t);
        }
    }

    @Override
    public <E> QueryResult<E> find(String tableName, Query query, Class<E> returnType) {
        try {
            checkEntity(returnType);
            Scan scan = queryTranslator.translate((HBaseQuery) query);
            ResultScanner result = getTable(tableName).getScanner(scan);
            return new HBaseQueryResult<>(returnType, result.iterator());
        } catch (Throwable t) {
            throw new RuntimeException("DAO Exception", t);
        }
    }

    @Override
    public QueryResults find(String tableName, Query query) {
        try {
            ResultScanner result = getTable(tableName).getScanner(queryTranslator.translate((HBaseQuery) query));
            closer.add(result);
            return new HBaseQueryResults(result);
        } catch (Throwable t) {
            throw new RuntimeException("DAO Exception", t);
        }
    }

    @Override
    public <E> E findOne(String tableName, Class<E> returnType, Query query) {
        try {
            ResultScanner result = getTable(tableName).getScanner(queryTranslator.translate((HBaseQuery) query));
            closer.add(result);
            Iterator<Result> results = result.iterator();
            return !results.hasNext()? null : new ResultTranslator<>(returnType).translate(results.next());
        } catch (Throwable t) {
            throw new RuntimeException("DAO Exception", t);
        }
    }

    @Override
    public void close() throws IOException {
        try {
            closer.add(table).close();
        } catch (Throwable throwable) {
            LOGGER.warn("Closer close with Throwable", throwable);
        }
    }

    private void checkEntity(Class<?> clazz) {
        if (!entityRegister.isRegister(clazz)) {
            throw new RuntimeException(String.format("The Entity %s has not be Register", clazz));
        }
    }

    private synchronized Table getTable(String tableName) throws IOException {
        if (table == null) {
            table = getConnection().getTable(TableName.valueOf(tableName));
        }
        return table;
    }

    protected abstract Connection getConnection();
}
