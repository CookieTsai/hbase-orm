package tw.cookie.orm;

import tw.cookie.orm.annotation.Entity;
import tw.cookie.orm.query.QueryResult;
import tw.cookie.orm.query.QueryResults;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HBaseQueryResults implements QueryResults {
    private static final Logger LOGGER = LoggerFactory.getLogger(HBaseQueryResults.class);

    private static final byte[] CQ_ENTITY = Bytes.toBytes("!entity");

    private final ResultScanner scanner;

    private Map<String, List<Result>> resultsMap;

    HBaseQueryResults(ResultScanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public <R> QueryResult<R> getEntity(Class<R> type) {
        return new HBaseQueryResult<>(type, getResultList(type).iterator());
    }

    @Override
    public <R> int getEntitySize(Class<R> type) {
        return getResultList(type).size();
    }

    private <R> List<Result> getResultList(Class<R> type) {
        if (resultsMap == null) {
            translate();
        }
        List<Result> results = resultsMap.get(getEntityName(type));
        if (results == null) {
            results = new ArrayList<>();
        }
        return results;
    }

    private synchronized void translate() {
        if (resultsMap != null) {
            return;
        }

        Map<String, List<Result>> resultsMap = new HashMap<>();
        for (Result result: scanner) {
            String entityName = getEntityName(result);
            if (entityName == null) {
                continue;
            }

            List<Result> results = resultsMap.get(entityName);
            if (results == null) {
                results = new ArrayList<>();
                resultsMap.put(entityName, results);
            }

            results.add(result);
        }
        this.resultsMap = resultsMap;
    }

    private <R> String getEntityName(Class<R> type) {
        Entity entity = type.getAnnotation(Entity.class);
        return entity == null || entity.name().equals("")? type.getSimpleName() : entity.name();
    }

    private String getEntityName(Result result) {
        String entityName = null;
        for (Cell cell: result.rawCells()) {
            if (Bytes.equals(CellUtil.cloneQualifier(cell), CQ_ENTITY)) {
                entityName = Bytes.toString(CellUtil.cloneValue(cell));
                break;
            }
        }
        if (entityName == null) {
            LOGGER.error("Performance Issue: Can't Not Match Entity Name From Result");
        }
        return entityName;
    }
}
