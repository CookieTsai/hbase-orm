package tw.cookie.orm.impl.entity;

import tw.cookie.orm.annotation.Entity;
import tw.cookie.orm.annotation.Id;

@Entity
public class User1 {

    @Id
    private String userId;

    private String name;

    private Integer age;

    public User1() {}

    public User1(String userId, String name, Integer age) {
        this.userId = userId;
        this.name = name;
        this.age = age;
    }

    public String getUserId() {
        return userId;
    }

    public User1 setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getName() {
        return name;
    }

    public User1 setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getAge() {
        return age;
    }

    public User1 setAge(Integer age) {
        this.age = age;
        return this;
    }
}
