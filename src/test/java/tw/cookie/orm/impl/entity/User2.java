package tw.cookie.orm.impl.entity;

import tw.cookie.orm.annotation.*;

@Entity
public class User2 {

    @EmbeddedId
    private RK rk;

    @Column
    private String note;

    public User2() {}

    public User2(String userId, String account, String note) {
        this.rk = new RK(userId, account);
        this.note = note;
    }

    public RK getRk() {
        return rk;
    }

    public User2 setRk(RK rk) {
        this.rk = rk;
        return this;
    }

    public String getNote() {
        return note;
    }

    public User2 setNote(String note) {
        this.note = note;
        return this;
    }

    @Embeddable
    public static class RK {

        @UseGenerator
        private String userId;

        private String account;

        public RK() {}

        public RK(String userId, String account) {
            this.userId = userId;
            this.account = account;
        }

        public String getUserId() {
            return userId;
        }

        public RK setUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public String getAccount() {
            return account;
        }

        public RK setAccount(String account) {
            this.account = account;
            return this;
        }
    }
}
