package tw.cookie.orm.impl.entity;

import tw.cookie.orm.annotation.Column;
import tw.cookie.orm.annotation.Entity;
import tw.cookie.orm.annotation.Id;

@Entity(cf = "test")
public class TestEntity {

    @Id
    private String id;

    @Column
    private byte[] bytes;

    @Column(name = "boolean")
    private Boolean aBoolean;

    @Column(name = "int")
    private Integer integer;

    @Column(name = "short")
    private Short aShort;

    @Column(name = "long")
    private Long aLong;

    @Column(name = "double")
    private Double aDouble;

    @Column(name = "float")
    private Float aFloat;

    @Column
    private String string;

    public TestEntity() {
    }

    public TestEntity(String id, byte[] bytes, Boolean aBoolean, Integer integer, Short aShort, Long aLong, Double aDouble, Float aFloat, String string) {
        this.id = id;
        this.bytes = bytes;
        this.aBoolean = aBoolean;
        this.integer = integer;
        this.aShort = aShort;
        this.aLong = aLong;
        this.aDouble = aDouble;
        this.aFloat = aFloat;
        this.string = string;
    }

    public String getId() {
        return id;
    }

    public TestEntity setId(String id) {
        this.id = id;
        return this;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public TestEntity setBytes(byte[] bytes) {
        this.bytes = bytes;
        return this;
    }

    public Boolean getaBoolean() {
        return aBoolean;
    }

    public TestEntity setaBoolean(Boolean aBoolean) {
        this.aBoolean = aBoolean;
        return this;
    }

    public Integer getInteger() {
        return integer;
    }

    public TestEntity setInteger(Integer integer) {
        this.integer = integer;
        return this;
    }

    public Short getaShort() {
        return aShort;
    }

    public TestEntity setaShort(Short aShort) {
        this.aShort = aShort;
        return this;
    }

    public Long getaLong() {
        return aLong;
    }

    public TestEntity setaLong(Long aLong) {
        this.aLong = aLong;
        return this;
    }

    public Double getaDouble() {
        return aDouble;
    }

    public TestEntity setaDouble(Double aDouble) {
        this.aDouble = aDouble;
        return this;
    }

    public Float getaFloat() {
        return aFloat;
    }

    public TestEntity setaFloat(Float aFloat) {
        this.aFloat = aFloat;
        return this;
    }

    public String getString() {
        return string;
    }

    public TestEntity setString(String string) {
        this.string = string;
        return this;
    }
}
