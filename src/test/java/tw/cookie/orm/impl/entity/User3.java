package tw.cookie.orm.impl.entity;

import tw.cookie.orm.annotation.Entity;
import tw.cookie.orm.annotation.Id;
import tw.cookie.orm.generator.GenType;

@Entity
public class User3 {

    @Id(genType = GenType.SHA_64)
    private String userId;

    public User3() {}

    public User3(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public User3 setUserId(String userId) {
        this.userId = userId;
        return this;
    }
}
