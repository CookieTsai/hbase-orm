package tw.cookie.orm.impl.util;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tw.cookie.orm.annotation.Entity;

public class HBaseUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(HBaseUtils.class);

    public static boolean createTable(String tableName) {
        return createTable(tableName, Entity.CF);
    }

    public static boolean createTable(String tableName, String... columnFamilies) {
        try (Admin admin = ConnectionHolder.getInstance().getConnection().getAdmin()) {
            TableName table = TableName.valueOf(tableName);
            if (!admin.tableExists(table)) {
                HTableDescriptor desc = new HTableDescriptor(table);
                for (String cf: columnFamilies) {
                    desc.addFamily(new HColumnDescriptor(cf));
                }
                admin.createTable(desc);
            } else {
                LOGGER.info("{} exists!", tableName);
            }
        } catch (Exception e) {
            LOGGER.error("Caught Exception:", e);
        }
        return true;
    }
}