package tw.cookie.orm.impl.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionHolder {
    private static final Logger LOG = LoggerFactory.getLogger(ConnectionHolder.class);

    private static final String ENV_ZOOKEEPER_QUORUM = "hbase.zookeeper.quorum";
    private static final String ENV_ROOT_DIR = "hbase.rootdir";

    private static final ConnectionHolder INSTANCE = new ConnectionHolder();

    private Connection CONNECTION;

    {
        Configuration hBaseConf = HBaseConfiguration.create();

        String zooKeeperQuorum = System.getProperty(ENV_ZOOKEEPER_QUORUM);
        String hdfsRootDir = System.getProperty(ENV_ROOT_DIR);

        LOG.info("zookeeper: {}", zooKeeperQuorum);
        LOG.info("hdfs: {}", hdfsRootDir);

        if (zooKeeperQuorum == null || zooKeeperQuorum.length() == 0) {
            zooKeeperQuorum = "localhost";
        }

        if (hdfsRootDir == null || hdfsRootDir.length() == 0) {
            hdfsRootDir = "hdfs://localhost/hbase";
        }

        hBaseConf.set(ENV_ZOOKEEPER_QUORUM, zooKeeperQuorum);
        hBaseConf.set(ENV_ROOT_DIR, hdfsRootDir);

        try {
            CONNECTION = ConnectionFactory.createConnection(hBaseConf);
        } catch (Exception e) {
            LOG.error("Catch Exception:", e);
            throw new RuntimeException(e);
        }
    }

    private ConnectionHolder() {}

    public Connection getConnection() {
        return CONNECTION;
    }

    public static ConnectionHolder getInstance() {
        return INSTANCE;
    }
}
