package tw.cookie.orm.impl;

import tw.cookie.orm.AbstractHBaseExecutor;
import tw.cookie.orm.impl.util.ConnectionHolder;
import org.apache.hadoop.hbase.client.Connection;

public class HBaseExecutor extends AbstractHBaseExecutor {

    @Override
    protected Connection getConnection()  {
        return ConnectionHolder.getInstance().getConnection();
    }
}
