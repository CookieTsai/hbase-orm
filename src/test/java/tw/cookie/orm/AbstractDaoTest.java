package tw.cookie.orm;

import tw.cookie.orm.impl.TestDao;
import tw.cookie.orm.impl.UserDao;
import tw.cookie.orm.impl.entity.TestEntity;
import tw.cookie.orm.impl.entity.User1;
import tw.cookie.orm.impl.entity.User2;
import tw.cookie.orm.impl.entity.User3;
import tw.cookie.orm.impl.util.HBaseUtils;
import tw.cookie.orm.query.Query;
import tw.cookie.orm.query.QueryResult;
import tw.cookie.orm.query.QueryResults;
import tw.cookie.orm.register.EntityRegister;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.hadoop.hbase.client.Scan;
import org.junit.*;
import org.slf4j.*;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static tw.cookie.orm.query.criterion.Restrictions.*;
import static tw.cookie.orm.query.criterion.Selector.ifEmpty;
import static tw.cookie.orm.query.criterion.Selector.ifNull;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class AbstractDaoTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDaoTest.class);

    private static final HBaseQueryTranslator queryTranslator = new HBaseQueryTranslator();

    @BeforeClass
    public static void beforeClass() throws Exception {
        HBaseUtils.createTable("User");
        HBaseUtils.createTable("Test", "test");

        // Register Entities
        EntityRegister register = EntityRegister.getInstance();
        register.register(User1.class);
        register.register(User2.class);
        register.register(User3.class);
        register.register(TestEntity.class);

        try (UserDao userDao = new UserDao();
             TestDao testDao = new TestDao()) {

            List<Object> list = new ArrayList<>();

            // Create User1
            list.add(new User1("U001", "User01", 22));
            list.add(new User1("U002", "User02", 23));
            list.add(new User1("U003", "User03", 24));
            list.add(new User1("U004", "User04", 25));
            list.add(new User1("U005", "User05", 26));
            list.add(new User1("U006", "User06", 27));

            // Create User2
            list.add(new User2("U001", "user01@account", "User01's Account"));
            list.add(new User2("U002", "user02@account", "User02's Account"));
            list.add(new User2("U003", "user03@account", "User03's Account"));
            list.add(new User2("U004", "user04@account", "User04's Account"));
            list.add(new User2("U005", "user05@account", "User05's Account"));
            list.add(new User2("U006", "user06@account", "User06's Account"));

            // Create User3
            list.add(new User3("U001"));
            list.add(new User3("U002"));
            list.add(new User3("U003"));
            list.add(new User3("U004"));

            userDao.save(list);

            testDao.save(new TestEntity("row1", null, null, null, null, null, null, null, null));
            testDao.save(new TestEntity("row2", "bytes".getBytes(StandardCharsets.UTF_8), true, 2, (short) 2,2L,2d,2.0f, ""));
            testDao.save(new TestEntity("row3", "bytes".getBytes(StandardCharsets.UTF_8), true, 3, (short) -3,-3L,-3d,-3.0f, "null"));
            testDao.save(new TestEntity("row4", "bytes".getBytes(StandardCharsets.UTF_8), true, 4, (short) 4,4L,4d,4.0f, "Something"));
        }
    }

    @AfterClass
    public static void afterClass() throws Exception {
        try (UserDao userDao = new UserDao();
             TestDao testDao = new TestDao()) {
            // Delete User1
            userDao.deleteShaRow(Arrays.asList("U001", "U002","U003", "U004","U005","U006"));

            // Delete User2
            userDao.delete(Arrays.asList(
                    new User2.RK("U001", "user01@account"),
                    new User2.RK("U002", "user02@account"),
                    new User2.RK("U003", "user03@account"),
                    new User2.RK("U004", "user04@account"),
                    new User2.RK("U005", "user05@account"),
                    new User2.RK("U006", "user06@account")));

            // Delete User3
            userDao.deleteSha64("U001");
            userDao.deleteSha64("U002");
            userDao.deleteSha64(Arrays.asList("U003", "U004"));

            // Delete TestEntity
            testDao.deleteShaRow("row1");
            testDao.deleteShaRow("row2");
            testDao.deleteShaRow("row3");
            testDao.deleteShaRow("row4");
        }
    }

    @Test
    public void queryBuilderTest() throws Exception {
        HBaseQuery query = new HBaseQueryBuilder()
                .addEntity(TestEntity.class)
                .setStartRow("startRow")
                .setStopRow("stopRow")
                .add( in("cf:name", null, "Cookie", (long) 1, 1, true, (double) 10, (short) 1, (float) 1) )
                .add( in("cf:name", Arrays.asList(null, "Cookie", (long) 1, 1, true, (double) 10, (short) 1, (float) 1)) )
                .add( notIn("cf:name", "Dog", "Cat", "Bird"))
                .add( notIn("cf:name", Arrays.asList("Dog", "Cat", "Bird")))
                .add( between("cf:age", "10", "40"))
                .add( eq("cf:status", "1"))
                .add( ne( "cf:name", "YoHe"))
                .add( gt("cf:age", "10"))
                .add( lt("cf:age", "40"))
                .add( gte("cf:age", "10"))
                .add( lte("cf:age", "40"))
                .add( isNull("cf:not_exist_cq"))
                .add( isNotNull("cf:address"))
                .add( isEmpty("cf:not_exist_cq"))
                .add( isNotEmpty("cf:address"))
                .add( or(eq("cf:name", "Cookie"), eq("cf:name", "Cowman")))
                .add( and(isNull("cf:not_exist_cq"), isNotNull("cf:address")))
                .add( substring("cf:name", "cookie"))
                .add( regexString("cf:name", "cookie"))
                .add( substringId("U001"))
                .add( regexStringId("U001"))
                .setBatch(-1)
                .setCaching(-1)
                .setTimeRange(Long.MIN_VALUE, Long.MAX_VALUE)
                .setReversed(true)
                .setLimit(100)
                .build();

        LOGGER.info("------- start translate --------");

        long startTime = System.currentTimeMillis();
        Scan scan = queryTranslator.translate(query);
        long spend = System.currentTimeMillis() - startTime;

        LOGGER.info("Spend Time: " + spend);
        assertThat(spend <= 200, is(true));

        LOGGER.info("------- stop translate --------");
        LOGGER.info(scan.toJSON());

        // Test HBaseQueryBuilder without EntityType
        scan = queryTranslator.translate(new HBaseQueryBuilder(false).build());
        LOGGER.info(scan.toJSON());
    }

    @Test
    public void getEntity() throws Exception {
        try (UserDao userDao = new UserDao()) {
            // Get User1
            User1 user1 = userDao.getByShaRowId("U010", User1.class);

            // Get User2
            User2 user2 = userDao.getById(new User2.RK("U001","user01@account"), User2.class);

            // Get User3
            User3 user3 = userDao.getBySha64Id("U001", User3.class);

            LOGGER.info(ToStringBuilder.reflectionToString(user1));
            LOGGER.info(ToStringBuilder.reflectionToString(user2));
            LOGGER.info(ToStringBuilder.reflectionToString(user3));

            assertThat(user1 == null, is(true));
            assertThat(user2 != null, is(true));
            assertThat(user3 != null, is(true));
        }
    }

    @Test
    public void findOne() throws Exception {
        try (UserDao userDao = new UserDao()) {
            Query query = new HBaseQueryBuilder()
                             .addEntity(User1.class)
                             .add(between("cf:age", 23, 26))
                             .build();

            User1 user1 = userDao.findOne(User1.class, query);

            LOGGER.info(ToStringBuilder.reflectionToString(user1));

            assertThat(user1 != null, is(true));
        }
    }

    @Test
    public void findOneEntityType() throws Exception {
        try (UserDao userDao = new UserDao()) {
            Query query = new HBaseQueryBuilder()
                             .addEntity(User1.class)
                             .add( between("cf:age", 23, 26) )
                             .build();

            QueryResult<User1> queryResult = userDao.find(query, User1.class);
            int cnt = 0;
            for (User1 user1: queryResult) {
                LOGGER.info(ToStringBuilder.reflectionToString(user1));
                cnt ++;
            }

            assertThat(cnt, is(4));
        }
    }

    @Test
    public void findMultiEntityType() throws Exception {
        try (UserDao userDao = new UserDao()) {
            Query query = new HBaseQueryBuilder()
                             .addEntity(User1.class)
                             .addEntity(User2.class)
                             .add( in("cf:userId", "U001", "U002", "U005") )
                             .build();

            QueryResults queryResults = userDao.find(query);

            for (User1 user1: queryResults.getEntity(User1.class)) {
                LOGGER.info(ToStringBuilder.reflectionToString(user1));
            }

            QueryResult<User2> user2s = queryResults.getEntity(User2.class);
            for (User2 user2: user2s) {
                LOGGER.info(ToStringBuilder.reflectionToString(user2));
            }

            assertThat(queryResults.getEntitySize(User1.class), is(3));
            assertThat(queryResults.getEntitySize(User2.class), is(3));

        }
    }

    @Test
    public void testColumnFamily() throws Exception {
        try (TestDao testDao = new TestDao()) {
            Query query = new HBaseQueryBuilder()
                    .addEntity(TestEntity.class)
                    .add( isNull("test:string"))
                    .build();

            int cnt = 0;
            for (TestEntity entity: testDao.find(query, TestEntity.class)) {
                LOGGER.info(ToStringBuilder.reflectionToString(entity));
                LOGGER.info(ifNull(entity.getString(), "This is Null"));
                cnt++;
            }

            assertThat(cnt , is(2));

            LOGGER.info("-------------這是分隔線------------");

            query = new HBaseQueryBuilder()
                    .addEntity(TestEntity.class)
                    .add( isEmpty("test:string"))
                    .build();

            cnt = 0;
            for (TestEntity entity: testDao.find(query, TestEntity.class)) {
                LOGGER.info(ToStringBuilder.reflectionToString(entity));
                LOGGER.info(ifEmpty(entity.getString(), "This is Empty"));
                cnt++;
            }

            assertThat(cnt , is(3));
        }
    }
}